-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : ven. 08 avr. 2022 à 16:42
-- Version du serveur : 10.4.22-MariaDB
-- Version de PHP : 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `catalogue`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id_categorie` int(10) NOT NULL,
  `identifiant` int(10) NOT NULL,
  `nom_categorie` varchar(255) NOT NULL,
  `photo_categorie` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id_categorie`, `identifiant`, `nom_categorie`, `photo_categorie`) VALUES
(1, 1637787030, 'Salonn', '1641414169.jpg'),
(2, 1637787035, 'Chambre', ''),
(3, 1637787039, 'Séjour', ''),
(4, 1641319845, 'lol', '1641410373.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `login`
--

CREATE TABLE `login` (
  `id_login` int(10) NOT NULL,
  `nom_login` varchar(25) NOT NULL,
  `pw_login` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `login`
--

INSERT INTO `login` (`id_login`, `nom_login`, `pw_login`) VALUES
(1, 'titi', '$2y$10$Gnz3VBRhaq5.0cwQbS6N5.1lY0vnUykHEMGy9TgO8MzlE59ohIPli');

-- --------------------------------------------------------

--
-- Structure de la table `photo`
--

CREATE TABLE `photo` (
  `id_photo` int(10) NOT NULL,
  `identifiant` int(10) NOT NULL,
  `nom_photo` varchar(255) NOT NULL,
  `chemin` varchar(255) NOT NULL,
  `identifiant_produit` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `photo`
--

INSERT INTO `photo` (`id_photo`, `identifiant`, `nom_photo`, `chemin`, `identifiant_produit`) VALUES
(2, 1639605477, '1639605477.jpg', './image/1639605477.jpg', 1639605477),
(4, 1641847848, '1641847848.jpg', './image/produit/1641847848.jpg', 1641847848),
(14, 1642456960, '1639602682.jpg', './image/produit/gallerie/1639602682.jpg', 1639602682),
(23, 1642540499, '1639605618.png', './image/produit/galerie/1639605618.png', 1639605618),
(24, 1642540504, '1639605618.jpg', './image/produit/galerie/1639605618.jpg', 1639605618),
(25, 1642540511, '1639605618.jpg', './image/produit/galerie/1639605618.jpg', 1639605618);

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE `produit` (
  `id_produit` int(10) NOT NULL,
  `identifiant` int(10) NOT NULL,
  `nom_produit` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `prix` varchar(255) NOT NULL,
  `stock` int(10) NOT NULL,
  `photo_produit` varchar(25) NOT NULL,
  `chemin_produit` varchar(50) NOT NULL,
  `id_sous_categorie` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`id_produit`, `identifiant`, `nom_produit`, `description`, `prix`, `stock`, `photo_produit`, `chemin_produit`, `id_sous_categorie`) VALUES
(1, 1639602682, 'lol', 'hbbbbbbbb', '200€', 55, '1639602682.JPG', './image/produit/1639602682.JPG', 6),
(3, 1639605618, 'Table basse', 'zdzadzadzaad', '400€', 48, '', '', 8),
(4, 1641847441, 'fgfgggggggg', 'ljlkjkjmlkmlk', '200€', 20, '', '', 3),
(5, 1641847789, 'fgfgggggggg', 'ljlkjkjmlkmlk', '200€', 20, '', '', 3),
(6, 1641847848, 'fgfgggggggg', 'ljlkjkjmlkmlk', '200€', 20, '', '', 3),
(7, 1641849080, 'dssds', 'sssssssssss', '150€', 50, '1641849080.jpg', './image/produit/164184908', 3),
(8, 1641850106, 'fsdfsfdsfddsdddfsf', 'fsdfsdfdsfssfsdf', '800€', 200, '1641850106.jpg', './image/produit/1641850106.jpg', 4);

-- --------------------------------------------------------

--
-- Structure de la table `sous_categorie`
--

CREATE TABLE `sous_categorie` (
  `id_sous_categorie` int(10) NOT NULL,
  `identifiant` int(10) NOT NULL,
  `nom_sous_categorie` varchar(255) NOT NULL,
  `photo_sous_categorie` varchar(25) NOT NULL,
  `id_categorie` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `sous_categorie`
--

INSERT INTO `sous_categorie` (`id_sous_categorie`, `identifiant`, `nom_sous_categorie`, `photo_sous_categorie`, `id_categorie`) VALUES
(3, 1637788608, 'clic clac', '', 1),
(4, 1637788615, 'Angles', '', 1),
(5, 1637788629, 'Armoires', '', 2),
(6, 1637788636, 'Lits', '', 2),
(7, 1637788644, 'Literies', '', 2),
(8, 1637788660, 'Living', '', 3),
(9, 1637788666, 'Tables', '', 3),
(10, 1637788672, 'Chaises', '', 3),
(11, 1641320799, 'loooooooool', '1641410781.jpg', 4),
(12, 1641320846, 'looooooooool', '1641414841.png', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id_categorie`);

--
-- Index pour la table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id_login`);

--
-- Index pour la table `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`id_photo`);

--
-- Index pour la table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`id_produit`),
  ADD KEY `id_sous_categorie` (`id_sous_categorie`);

--
-- Index pour la table `sous_categorie`
--
ALTER TABLE `sous_categorie`
  ADD PRIMARY KEY (`id_sous_categorie`),
  ADD KEY `id_categorie` (`id_categorie`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id_categorie` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `login`
--
ALTER TABLE `login`
  MODIFY `id_login` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `photo`
--
ALTER TABLE `photo`
  MODIFY `id_photo` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT pour la table `produit`
--
ALTER TABLE `produit`
  MODIFY `id_produit` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `sous_categorie`
--
ALTER TABLE `sous_categorie`
  MODIFY `id_sous_categorie` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `produit`
--
ALTER TABLE `produit`
  ADD CONSTRAINT `produit_ibfk_1` FOREIGN KEY (`id_sous_categorie`) REFERENCES `sous_categorie` (`id_sous_categorie`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `sous_categorie`
--
ALTER TABLE `sous_categorie`
  ADD CONSTRAINT `sous_categorie_ibfk_1` FOREIGN KEY (`id_categorie`) REFERENCES `categorie` (`id_categorie`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
