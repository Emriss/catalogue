<?php 
    require_once('./admin/include/connexion.php');
    if(isset($_POST['cat'])) {
        $catId = htmlspecialchars($_POST['cat']);
        $query = "SELECT DISTINCT * FROM sous_categorie WHERE id_categorie = :id";
        $req = $bdd->prepare($query);
        $req->bindValue(':id', $catId, );
        $req->execute();
        $results = $req->fetchAll();
        $content = '';
        $content .= '<ul>';
        foreach($results as $result) {
         
                
            $content .= '<li><a href="index.php?categorie='.$catId.'&souscat='.$result['id_sous_categorie'].'">'.$result['nom_sous_categorie'].'</a></li>';
                

        
        }
        $content .= '</ul>';
        echo $content;
    }

?>