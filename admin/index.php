<?php 
    include('./include/connexion.php');
    session_start();
    if(isset($_GET['disconnect'])) {
        $deco = $_GET['disconnect'];
        if($deco === '1') {
            unset($_SESSION['connected']);
            header("Location: ./index.php?page=4");
        }
    }
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="./css/style.css?v=<?php echo time();?>">
    <title>Back-office catalogue</title>
</head>
<body>
    <?php 
    $query = "SELECT * FROM `login`";

    $req = $bdd -> prepare($query);
    $req -> execute();
   
    while( $result_log = $req -> fetch()) {
        $login = $result_log['nom_login'];
        $pw = $result_log['pw_login'];
    }
        if(isset($_POST['nomCompte'])) {
            $sel = '55';
            $login_input = htmlspecialchars($_POST['nomCompte']);
            $pass_input = htmlspecialchars($_POST['passw']);
            if($login_input === $login && password_verify($pass_input,$pw)) {
                $_SESSION['connected'] = 1;
            }
        }

        if(isset($_SESSION['connected']) && $_SESSION['connected'] === 1 ) {
    ?>
    <aside>
        <div class="asideContainer">
            <a href="./index.php"><img class="logo" src="./image/thumb2-albanian-metal-flag-grunge-art-european-countries-national-symbols-albania-flag.jpg" alt=""></a>  
            <hr>
            <ul class="asideList">
                <li>
                    <a href="index.php?page=1">Catégorie</a>
                </li>
                <li>
                    <a href="index.php?page=2">Sous-catégorie</a>
                </li>
                <li>
                    <a href="index.php?page=3">Produit</a>
                </li>
            </ul>
        </div>
    </aside>
    <div class="masterContainer">
        <header>
            <ul class="breadcrumb">
                <?php 
                    $breadcrumb = '<li>
                                        <a href="./index.php">Accueil</a>
                                    </li>';

                    if(isset($_GET['page'])) {
                        $page = intval($_GET['page']);
                        switch($page) {

                            case 1 : 
                                $breadcrumb .= '<li>
                                                    <a href="./index.php?page=1">Catégorie</a>
                                                </li>';
                                if(isset($_GET['section']) && $_GET['section'] == 1) {
                                    $breadcrumb .= '<li>
                                                        <a href="./index.php?page=1&section=1">Saisie</a>
                                                    </li>';
                                
                                } elseif (isset($_GET['section']) && $_GET['section'] == 2) {
                                    $breadcrumb .= '<li>
                                                        <a href="./index.php?page=1&section=2">Gestion</a>
                                                    </li>';
                                }
                            break;

                            case 2 : 
                                $breadcrumb .= '<li>
                                                    <a href="./index.php?page=1">Sous-catégorie</a>
                                                </li>';
                                if(isset($_GET['section']) && $_GET['section'] == 1) {
                                    $breadcrumb .= '<li>
                                                        <a href="./index.php?page=2&section=1">Saisie</a>
                                                    </li>';
                                
                                } elseif (isset($_GET['section']) && $_GET['section'] == 2) {
                                    $breadcrumb .= '<li>
                                                        <a href="./index.php?page=2&section=2">Gestion</a>
                                                    </li>';
                                }               
                            break;

                            case 3 : 
                                $breadcrumb .= '<li>
                                                    <a href="./index.php?page=3">Produit</a>
                                                </li>';
                                if(isset($_GET['section']) && $_GET['section'] == 1) {
                                    $breadcrumb .= '<li>
                                                        <a href="./index.php?page=3&section=1">Saisie</a>
                                                    </li>';
                                
                                } elseif (isset($_GET['section']) && $_GET['section'] == 2) {
                                    $breadcrumb .= '<li>
                                                        <a href="./index.php?page=3&section=2">Gestion</a>
                                                    </li>';
                                }
                            break;

                            default : 
                                $breadcrumb .= '<li>
                                                    <a href="./index.php">Accueil</a>
                                                </li>';
                            break;                   
                        }
                    }
                    echo $breadcrumb;
                ?>
            </ul>
            <a class="btnDisc" href="./index.php?disconnect=1">Déconnexion</a>
        </header>
        <main>
            <?php 
                if(isset($_GET['page'])) {
                   $page = intval($_GET['page']);
                   $main_list2 = '<ul class="mainList">
                                    <li>
                                        <a href="?page='.$page.'&section=1">Saisie</a>
                                    </li>
                                    <li>
                                        <a href="?page='.$page.'&section=2">Gestion</a>
                                    </li>
                                </ul> ';
                    echo $main_list2;
                    switch($page) {
                        case 1 : 
                            
                            include('./include/categorie.php');
                            break;
    
                        case 2 : 
                            
                            include('./include/sous_categorie.php');
                            break;
                        
                        case 3 : 
                            
                            include('./include/produit.php');
                            break;
                            
                        default : 

                            break;                   
                    }
                } else {
                    $main_list = '<ul class="mainList">
                            <li>
                                <a href="index.php?page=1">Catégorie</a>
                            </li>
                            <li>
                                <a href="index.php?page=2">Sous-catégorie</a>
                            </li>
                            <li>
                                <a href="index.php?page=3">Produit</a>
                            </li>
                        </ul>';
                    echo $main_list;
                }
                
              
              
            ?>
        </main>
    </div>
    <?php  
        } elseif (isset($login_input) && $login_input != $login && !password_verify($pass_input,$pw)) {
            echo '<script language="javascript">';
            echo 'alert("Nom de compte ou mot de passe incorrect")';
            echo '</script>';       
            include('./include/login.php');
        } else {
            include('./include/login.php');
            
        }
    ?>
    <script src="./js/script.js"></script>
</body>
</html>
