<?php
    switch($page) {
        case 1 :
            if(isset($_GET['delete'])) {

                $action = intval($_GET['delete']);
                $query = "DELETE FROM `categorie` WHERE `id_categorie` = :id";
                $req = $bdd -> prepare($query);
                $req -> bindValue(':id',$action, PDO::PARAM_INT);
                $req -> execute();
                header("Location: ./index.php?page=1&section=2");
    
            } elseif(isset($_GET['update'])) {
    
                $action = intval($_GET['update']);
                $query = "SELECT * FROM categorie WHERE `id_categorie` =".$action;
                $req = $bdd -> prepare($query);
           
                $req -> execute();
                $resultUp = $req -> fetch();
                include('./include/gestionForm.php');
                $query = "SELECT * FROM categorie WHERE id_categorie = :id";
                $req = $bdd -> prepare($query);
                $req -> bindValue(':id',$action, PDO::PARAM_STR);
              
                if(isset($_POST['up'])){
    
                    $upNom = $_POST['nom_categorie'];

                    if(isset($_FILES['photo']) && $_FILES['photo']['name'] != NULL) {

   
                        $dossier = 'image/categorie'; 
                        $nom_photo = $resultUp['identifiant'];
                        $extensions_valides = array('jpg', 'JPG', 'jpeg', 'JPEG', 'PNG', 'png');
                        $extension_upload = substr(strrchr($_FILES['photo']['name'],'.'),1);
         
                        if(in_array($extension_upload,$extensions_valides)) {
                            $old_picture = unlink('./image/produit'.'/'.$resultUp['photo_categorie']);
                            $nom_photo = $nom_photo.'.'.$extension_upload;
                            $chemin = './'.$dossier.'/'.$nom_photo;
                            
                            $resultat = move_uploaded_file($_FILES['photo']['tmp_name'], $chemin);
                        }
                    } else {
                        $nom_photo = $resultUp['photo_categorie'];
                    }
                   
                
                    $query = "UPDATE categorie SET nom_categorie = :nom, photo_categorie = :photo  WHERE `id_categorie` = :id";
                    $req = $bdd -> prepare($query);
                    $req -> bindValue(':id',$action, PDO::PARAM_INT);
                    $req -> bindValue(':nom',$upNom, PDO::PARAM_STR);
                    $req -> bindValue(':photo',$nom_photo, PDO::PARAM_STR);
   
                    $req -> execute();
                    header('Location: ./index.php?page=1&section=2');  
                } 
                    
            } else {
                $query = "SELECT * FROM categorie";
                $req = $bdd -> prepare($query);
            } 

            $req -> execute();
            $result = $req -> fetchAll();
            $contenueGestion = '';
            
            foreach($result as $results) {
            $contenueGestion .= "<div class='containerGestion'>";
            $contenueGestion .= "<div class='col'>".$results['identifiant']."</div>";
            $contenueGestion .= "<div class='col'>".$results['nom_categorie']."</div>";
            $contenueGestion .= '<div class="col"><p class="imgOver">'.$results['photo_categorie'].'</p></div>';
            $contenueGestion .= "<div class='colAction'>
                                    <a href='./index.php?page=1&section=2&update=".$results['id_categorie']."'><i class='fas fa-cog'></i></a>
                                </div>";
                                
            $contenueGestion .= "<div class='colAction'>
                                    <a class='deleteLink' href='./index.php?page=1&section=2&delete=".$results['id_categorie']."'><i class='far fa-trash-alt'></i></a>
                                </div>";
            $contenueGestion.= "</div>";
            }
            echo $contenueGestion;
        break; 

        case 2 :
            if(isset($_GET['delete'])) {

                $action = intval($_GET['delete']);
                $query = "DELETE FROM `sous_categorie` WHERE `id_sous_categorie` = :id";
                $req = $bdd -> prepare($query);
                $req -> bindValue(':id',$action, PDO::PARAM_INT);
                $req -> execute();
                header("Location: ./index.php?page=2&section=2");
    
            } elseif(isset($_GET['update'])) {
    
                $action = intval($_GET['update']);
                $query = "SELECT * FROM sous_categorie WHERE `id_sous_categorie` = :id";
                $req = $bdd -> prepare($query);
                $req -> bindValue(':id',$action, PDO::PARAM_INT);
                $req -> execute();
        
                $resultUp = $req -> fetch();
                include('./include/gestionForm.php');

                $req -> bindValue(':id',$action, PDO::PARAM_INT);

                if(isset($_POST['up'])){
    
                    $upNom = $_POST['nom_sous_categorie'];

                    if(isset($_FILES['photo']) && $_FILES['photo']['name'] != null) {

                        $dossier = 'image/sous_categorie'; 
                        $nom_photo = $resultUp['identifiant'];
                        $extensions_valides = array('jpg', 'JPG', 'jpeg', 'JPEG', 'PNG', 'png');
                        $extension_upload = substr(strrchr($_FILES['photo']['name'],'.'),1);
         
                        if(in_array($extension_upload,$extensions_valides)) {
                            $old_picture = unlink('./image/produit'.'/'.$resultUp['photo_sous_categorie']);
                            $nom_photo = $nom_photo.'.'.$extension_upload;
                            $chemin = './'.$dossier.'/'.$nom_photo;
                            $resultat = move_uploaded_file($_FILES['photo']['tmp_name'], $chemin);
                        }
                    } else {
                        $nom_photo = $resultUp['photo_sous_categorie'];
                    }
                    $query = "UPDATE sous_categorie SET nom_sous_categorie = :nom, photo_sous_categorie = :photo  WHERE `id_sous_categorie` = :id";
                    $req = $bdd -> prepare($query);

                    $req -> bindValue(':id',$action, PDO::PARAM_INT);
                    $req -> bindValue(':nom',$upNom, PDO::PARAM_STR);
                    $req -> bindValue(':photo',$nom_photo, PDO::PARAM_STR);
                    $req -> execute();
                    header('Location: ./index.php?page=2&section=2');  
    
                } 

            } else {
                $query = "SELECT * FROM sous_categorie ORDER BY sous_categorie.id_categorie ASC";
                $req = $bdd -> prepare($query);
            } 

            $contenueGestion = '';
            $query = "SELECT * FROM `sous_categorie` ORDER BY id_sous_categorie ASC" ;
            
            $req -> execute();
            $result = $req -> fetchAll();
            
            foreach($result as $results) {
                $contenueGestion .= "<div class='containerGestion'>";
                $contenueGestion .= "<div class='col'>".$results['identifiant']."</div>";
                $contenueGestion .= "<div class='col'>".$results['nom_sous_categorie']."</div>";
                $contenueGestion .= '<div class="col"><p class="imgOver">'.$results['photo_sous_categorie'].'</p></div>';
                $contenueGestion .= "<div class='colAction'>
                                        <a href='./index.php?page=2&section=2&update=".$results['id_sous_categorie']."' ><i class='fas fa-cog'></i></a>
                                    </div>";
                                    
                $contenueGestion .= "<div class='colAction'>
                                        <a class='deleteLink' href='./index.php?page=2&section=2&delete=".$results['id_sous_categorie']."'><i class='far fa-trash-alt'></i></a>
                                    </div>";
                $contenueGestion.= "</div>";
            }
            echo $contenueGestion;

        break;

        case 3 :
            if(isset($_GET['delete'])) {

                $action = intval($_GET['delete']);
                $query = "DELETE FROM `produit`  WHERE `identifiant` = :id";
                
                $req = $bdd -> prepare($query);
                $req -> bindValue(':id',$action, PDO::PARAM_INT);
   
                $req -> execute();
                header("Location: ./index.php?page=3&section=2");
    
            } elseif(isset($_GET['update'])) {
    
                $action = intval($_GET['update']);
                $query = "SELECT * FROM produit INNER JOIN photo WHERE produit.identifiant = :id";
                $req = $bdd -> prepare($query);
                $req -> bindValue(':id',$action, PDO::PARAM_INT);
                $req -> execute();

                $resultUps = $req->fetchAll();
                foreach($resultUps as $resultUp) { 
                    include_once('./include/gestionForm.php'); 
                   
                }
                var_dump($resultUp);
                

                if(isset($_GET['delimg'])) {
                    $delete_image = $_GET['delimg'];

                    $query = 'DELETE FROM photo WHERE id_photo = :id';
                    $req = $bdd -> prepare($query);
                    $req -> bindValue(':id',$delete_image, PDO::PARAM_INT);
                    $req -> execute();
                 
                }
                
                    if(isset($_FILES['photo_gallerie']) && $_FILES['photo_gallerie']['name'] != null) {
                        echo ($_FILES['photo_gallerie']['name']);
                        $dossier = 'image/produit'; 
                        $identifiant = time();
                        $nom_photo = $identifiant;
                        $extensions_valides = array('jpg', 'JPG', 'jpeg', 'JPEG', 'PNG', 'png');
                        $extension_upload = substr(strrchr($_FILES['photo_gallerie']['name'],'.'),1);
         
                        if(in_array($extension_upload,$extensions_valides)) {
                            $nom_photo = $nom_photo.'.'.$extension_upload;
                            $chemin = './'.$dossier.'/'.$nom_photo;
                            $resultat = move_uploaded_file($_FILES['photo_gallerie']['tmp_name'], $chemin); 

                            $query = "INSERT INTO photo VALUES (:id,:ident,:nom,:chemin,:fk)";
                            $req = $bdd -> prepare($query);
                        


            
                            $req -> bindValue(':id','', PDO::PARAM_INT);
                            $req -> bindValue(':ident',$identifiant, PDO::PARAM_INT);
                            $req -> bindValue(':nom',$nom_photo, PDO::PARAM_STR);
                            $req -> bindValue(':chemin',$chemin, PDO::PARAM_STR);
                            $req -> bindValue(':fk',$resultUp['identifiant'], PDO::PARAM_INT);

                            $req->execute();
                        }

                    }

                

                if(isset($_POST['up'])){
    
                    $up_nom= htmlspecialchars($_POST['nom_produit']);
                    $up_desc = htmlspecialchars($_POST['description']);
                    $up_prix = htmlspecialchars($_POST['prix']);
                    $up_stock = htmlspecialchars($_POST['stock']);
                   
                    $query = "UPDATE produit SET 
                    nom_produit = :nom, `description` = :descr, prix = :prix, stock = :stock 
                    WHERE `identifiant` = :id";
                    $req = $bdd -> prepare($query);
                    $req -> bindValue(':id',$action, PDO::PARAM_INT);
                    $req -> bindValue(':nom',$up_nom, PDO::PARAM_STR);
                    $req -> bindValue(':descr',$up_desc, PDO::PARAM_STR);
                    $req -> bindValue(':prix',$up_prix, PDO::PARAM_STR);
                    $req -> bindValue(':stock',$up_stock, PDO::PARAM_INT);
   
                    $req -> execute();
                    header('Location: ./index.php?page=3&section=2');  
                } 
    
            } else {
                $query = "SELECT *  FROM produit ORDER BY produit.id_sous_categorie ASC";
                $req = $bdd -> prepare($query);
            } 
        
            $contenueGestion = '';
          
            $req -> execute();
            $result = $req -> fetchAll();
?>
<div class="productContainer">  
    <div id='imgDisplay'></div> 
    <?php
            
        foreach($result as $results) {
            $contenueGestion .= "<div class='product'>";
            $contenueGestion .= "<div class='col'>Timestamp : ".$results['identifiant']."</div>";
            $contenueGestion .= "<div class='col'>Nom : ".$results['nom_produit']."</div>";
            $contenueGestion .= "<div class='col'>Description : ".$results['description']."</div>";
            $contenueGestion .= '<div class="col"><p class="imgOver">'.$results['chemin_produit'].'</p></div>';
            
            $contenueGestion .= "<div class='col'>Prix : ".$results['prix']."</div>";
            $contenueGestion .= "<div class='col'>Stock : ".$results['stock']."</div>";
            $contenueGestion .= "<div class='colAction'>
                                    <a href='./index.php?page=3&section=2&update=".$results['identifiant']."' ><i class='fas fa-cog'></i></a>
                                </div>";
                                    
            $contenueGestion .= "<div class='colAction'>
                                    <a class='deleteLink' href='./index.php?page=3&section=2&delete=".$results['identifiant']."'><i class='far fa-trash-alt'></i></a>
                                </div>";
            $contenueGestion.= "</div>";
        }
        echo $contenueGestion;
        break;
        }
    ?>
</div>
