<?php 
    switch($page) {

        case 1 : 

            if(isset($_POST['insert'])) {
                
                $idTime = intval(time());
                $nom = htmlspecialchars($_POST['nom_categorie']);
            
                if(isset($_FILES['photo']) && $_FILES['photo'] != null) {

                    echo ($_FILES['photo']['name']);
                    $dossier = 'image/categorie'; 
                    $nom_photo = $idTime;
                    $extensions_valides = array('jpg', 'JPG', 'jpeg', 'JPEG');
                    $extension_upload = substr(strrchr($_FILES['photo']['name'],'.'),1);
     
                    if(in_array($extension_upload,$extensions_valides)) {
                        $nom_photo = $nom_photo.'.'.$extension_upload;
                        $chemin = './'.$dossier.'/'.$nom_photo;
                        $resultat = move_uploaded_file($_FILES['photo']['tmp_name'], $chemin);
                    }
                }
                $query = "INSERT INTO `categorie` VALUES (:id,:ident,:nom, :photo)";

                $req = $bdd -> prepare($query);
                $req -> bindValue(':id','', PDO::PARAM_INT);
                $req -> bindValue(':ident',$idTime, PDO::PARAM_INT);
                $req -> bindValue(':nom',$nom, PDO::PARAM_STR);
                $req -> bindValue(':photo',$nom_photo, PDO::PARAM_STR);
                $req -> execute();
            }

            echo '<form class="formSaisie" action="#" method="POST" enctype="multipart/form-data">
                        <h2>Saisir votre catégorie</h2>
                        <input type="text" name="nom_categorie" placeholder="Saisir le nom de la catégorie">
                        <input type="file" name="photo">
                        <button name="insert">Valider</button>
                </form>';
            break;

        case 2 : 

            if (isset($_POST['insert'])) {
                
                $idTime = intval(time());
                $nom = htmlspecialchars($_POST['nomSouscategorie']);
                $id_categorie = htmlspecialchars($_POST['id_categorie']);
                
                if(isset($_FILES['photo']) && $_FILES['photo'] != null) {

                    echo ($_FILES['photo']['name']);
                    $dossier = 'image/sous_categorie'; 
                    $nom_photo = $idTime;
                    $extensions_valides = array('jpg', 'JPG', 'jpeg', 'JPEG');
                    $extension_upload = substr(strrchr($_FILES['photo']['name'],'.'),1);
     
                    if(in_array($extension_upload,$extensions_valides)) {
                        $nom_photo = $nom_photo.'.'.$extension_upload;
                        $chemin = './'.$dossier.'/'.$nom_photo;
                        $resultat = move_uploaded_file($_FILES['photo']['tmp_name'], $chemin);
                    }
                }
      

                $query = "INSERT INTO `sous_categorie` VALUES (:id,:ident,:nom,:photo,:id_categorie)";

                $req = $bdd -> prepare($query);
                $req -> bindValue(':id','', PDO::PARAM_INT);
                $req -> bindValue(':ident',$idTime, PDO::PARAM_INT);
                $req -> bindValue(':nom',$nom, PDO::PARAM_STR);
                $req -> bindValue(':photo',$nom_photo, PDO::PARAM_STR);
                $req -> bindValue(':id_categorie',$id_categorie, PDO::PARAM_INT);
                $req -> execute();
               

            }
            $query = "SELECT `id_categorie`, `nom_categorie` FROM `categorie`";
            $req = $bdd -> prepare($query);
            $req -> execute();
            $contenue = '';
            while($result = $req->fetch()){
                $contenue .= '<option value="'.$result['id_categorie'].'">'.$result['nom_categorie'].'</option>';
            }

            echo '<form class="formSaisie" action="#" method="POST" enctype="multipart/form-data">
                        <h2>Ajouter une sous-catégorie</h2>
                        <select name="id_categorie" title="catégorie">
                            '.$contenue.'
                        </select>
                        <input type="text" name="nomSouscategorie" placeholder="Saisir le nom de la sous-catégorie">
                        <input type="file" name="photo">
                        <button name="insert">Valider</button>
                    </form>';

            break;

        case 3 : 

            if (isset($_POST['insert3'])) { 
                $idTime = intval(time());
                $nom = htmlspecialchars($_POST['nomProduit']);
                $description = htmlspecialchars($_POST['description']);
                $prix = htmlspecialchars($_POST['prix']);
                $stock = intval($_POST['stock']);
                $fk = intval($_POST['sous_categorie']);
           
                if(isset($_FILES['photo']) && $_FILES['photo']['name'] != null) {
                    echo ($_FILES['photo']['name']);
                    $dossier = 'image/produit'; 
                    $identifiant = time();
                    $nom_photo = $identifiant;
                    $extensions_valides = array('jpg', 'JPG', 'jpeg', 'JPEG', 'PNG', 'png');
                    $extension_upload = substr(strrchr($_FILES['photo']['name'],'.'),1);
     
                    if(in_array($extension_upload,$extensions_valides)) {
                        $nom_photo = $nom_photo.'.'.$extension_upload;
                        $chemin = './'.$dossier.'/'.$nom_photo;
                        $resultat = move_uploaded_file($_FILES['photo']['tmp_name'], $chemin);

                        if($resultat) {
                           
                        }
                        $query = "INSERT INTO produit VALUES (:id,:ident,:nom,:descri,:prix,:stock,:nom_photo,:chemin,:fk)";
                        $req = $bdd -> prepare($query);
                      


        
                        $req -> bindValue(':id','', PDO::PARAM_INT);
                        $req -> bindValue(':ident',$idTime, PDO::PARAM_INT);
                        $req -> bindValue(':nom',$nom, PDO::PARAM_STR);
                        $req -> bindValue(':descri',$description, PDO::PARAM_STR);
                        $req -> bindValue(':prix',$prix, PDO::PARAM_STR);
                        $req -> bindValue(':stock',$stock, PDO::PARAM_INT);
                        $req -> bindValue(':nom_photo',$nom_photo, PDO::PARAM_STR);
                        $req -> bindValue(':chemin',$chemin, PDO::PARAM_STR);
                        $req -> bindValue(':fk',$fk, PDO::PARAM_INT);
                        $req -> execute();

                        }
                    }
                }

            

                   
            

            $query = 'SELECT categorie.id_categorie, `nom_categorie` FROM categorie';
            $req = $bdd -> prepare($query);
            $req -> execute();
            $contenue = '';
            $contenue2 = '';
            while($result = $req->fetch()){
                $contenue .= '<option value="'.$result['id_categorie'].'">'.$result['nom_categorie'].'</option>';
            }
            
            echo '<form id="produitForm" class="formSaisie" action="#" method="POST" enctype="multipart/form-data">
                        <h2>Saisir votre produit</h2>
                        <select type="button" ONCHANGE="submitForm()" id="categorieSelect" name="categorie" title="catégorie">
                            <option selected="selected">Choisir une catégorie</option>
                            '.$contenue.'
                        </select>
                        <select id="categorieSelect2"  name="sous_categorie" id="" title="sous-catégorie">
                        <option selected="selected">Choisir une sous-catégorie</option>

                        </select>
                        <input type="text" name="nomProduit" placeholder="Saisir le nom du produit">
                        <textarea name="description" id="" cols="30" rows="10" placeholder="Saisir la description du produit"></textarea>
                        <input type="text" name="prix" placeholder="Saisir le prix en €">
                        <input type="number" id="inputNumb" name="stock" placeholder="Saisir le nbre de produits">
                        <input type="file" name="photo" accept="image/png, image/jpeg, image/jpg">
                        <button name="insert3">Valider</button>
                </form> ';
                
        break;

        default : 
            echo '<form class="formSaisie" action="#" method="POST">
                        <h2>Saisir votre produit</h2>
                        <select name="categorie" title="catégorie">
                            <option value="categorie 1">categorie1</option>
                            <option value="categorie 2">categorie2</option>
                        </select>
                        <select name="sous-categorie" id="" title="sous-catégorie">
                            <option value="sous-categorie 1">sous-categorie1</option>
                            <option value="sous-categorie 2">sous-categorie2</option>
                        </select>
                        <input type="text" name="nomProduit" placeholder="Saisir le nom du produit">
                        <textarea name="description" id="" cols="30" rows="10" placeholder="Saisir la description du produit"></textarea>
                        <input type="text" name="prix" placeholder="Saisir le prix en €">
                        <input type="number" id="inputNumb" name="stock" placeholder="Saisir le nbre de produits">
                        <input type="file" name="photo" accept="image/png, image/jpeg, image/jpg">
                        <button name="insert3">Valider</button>
                </form>';
        break;
    } 
?>
