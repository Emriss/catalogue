<?php
    include('./admin/include/connexion.php');
    $query = "SELECT * FROM categorie";
    $req = $bdd->prepare($query);
    $req->execute();
    $results = $req->fetchAll();
    $id = array();
    $contenue = '';
    foreach($results as $result) {
        $contenue .= '<li id="firstLi" class="firstElement"><a data-id="'.$result['id_categorie'].'" href="./index.php?categorie='.$result['id_categorie'].'">'.$result['nom_categorie'].'</a></li>'; 
        $image[$result['id_categorie']] = $result['photo_categorie'];
        $id[$result['id_categorie']] = $result['id_categorie'];
    }
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css?v=<?php echo time();?>" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="./css/style.css?v=<?php echo time(); ?>">
    <title>Accueil</title>
</head>
<body>
<header>
    <div class="container">
        <a href="/"><img src="./img/logo-immo-1.png"></a>
        <span>FURNITÜREN, die ganze Möbel SHOP  </span>
    </div>
</header>
<nav>
    <div class="container">
    <div class="burgerMenu" id="menuBurger">
            <i class="fas fa-bars"></i>
        </div>
        <ul class="firsList" id="listFirst">
           
                    <?php 
                        echo $contenue;
                    ?>
           
           
            

        </ul>
       
    </div>
</nav>
<main>
    <div class="mainContainer">
    <?php 
  
        isset($_GET['categorie']) ? $img = '<img src="./admin/image/categorie/'.$image[$_GET["categorie"]].'">' : $img = '<img src="./img/img0556.jpg">';
        echo $img;
          
       
       
        ?>
    </div>
    <div class="mainContainer2">

        <?php
           if(isset($_GET['categorie'])) {
               $categorie = $_GET['categorie'];
               foreach($results as $result) {
                $categorie == $result['id_categorie'] ? $checkValue = 1 : null;
                } 
                if($checkValue == 1) {
         
                    require_once('./includes/sous-categorie.php');
                } else {
                
                    echo "<script> window.location = 'index.php';</script>";
                }
            } else {
                require_once('./includes/categorie.php');
            }
               
                    



        



        ?>
    </div>
</main>
<footer>
    <div class="footerContainer">
        <div>
            <img src="./img/logo-immo-1.png" alt="">
        </div>
        <ul>
            <li>À propos de Fürnituren</li>
            <li><a href="">Qui somme nous et nos engagements</a></li>
            <li><a href="">Mentions légales</a></li>
            <li><a href="">Moyens de paiement</a></li>
            <li><a href="">Livraison</a></li>
            <li><a href="">Conditions générales de ventes</a></li>
        </ul>
        <ul>
            <li>Aide et contact</li>
            <li><a href="">Formulaire de contact</a></li>
            <li><a href="">Plan du site</a></li>
            <li><a href="">Promotions meubles</a></li>
           
        </ul>
    </div>
</footer>
<script src="./js/script.js"></script>
</body>
</html>